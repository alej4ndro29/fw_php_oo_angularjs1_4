# Introducción <img src="doc/img/icon.png" width="25"/>

Aplicación para mantener el control de recursos en una librería.

Aplicación MVC (PHP, JS, AngularJS, OO)
- Proyecto de clase 1DAW.
- Aplicación PHP, AngularJS1.4, OO, APIs, Bootstrap.

<br>

# Configuración 
 - Modificar backend/model/db/config_ini con el nombre "config_ini".
 - Modificar backend/config/constants.template.php con el nombre "constants.template.php".
 
   Completar los parámetros que estan vacíos en los ficheros anteriores.
 - Configurar una base de datos con el sql doc/sql/Dump20190504.sql


<br>

# Características

### Home
- Lista con los elementos disponibles en base de datos.
- Posibilidad de filtrar los elementos por tipo, género o texto.
- Acceder a los detalles de cada artículo.

### Detalles
- Muestra información sobre el producto.
- Carga productos relacionados a través de API (Google Books).

### CRUD
 - Módulo exclusivo para usuarios administradores.
 - Posibilidad de completar el formulario de creación a través de la API de google books.

### Order
- Tabla con paginación con los recursos en base de datos (muestra cantidad de likes en cada artículo).
- Posibilidad de dar like cuando el usuario ha iniciado sesión.
- Boton para añadir un elemento al carrito (aumenta la cantidad si ya estaba en el carrito).

### Carrito
- En caso de estar vacío aparece un mensaje y la opción de mostrar los productos.
- Si hay elementos los muestra junto al precio y la cantidad.
- Tenemos la posibilidad de quitar el producto y modificar la cantidad (nunca negativa).

### Login
- Formulario para iniciar sesión y registrarse.
- Botón de recuperar contraseña.
- Social login.

### Profile
- Posibilidad de cambiar datos del usuario a través de un formulario.
- Dependent dropdown para seleccionar provincia y población.
- Datepicker para seleccinar la fecha de nacimiento.
- Tabs para ver los likes y las compras con paginación.

### Contacto
- Formulario de contacto que envía un correo electrónico.
- Google Maps.

Tests en el frontend y backend.

<br>

# Frontend # Detalles técnicos 

- Router en el fichero app.js
- Sistema de token para realizar acciones en el servidor con el usuario validado.
- Social login en Auth0 a través de un popup.

### Directivas AngularJS
- Barra de menú diferente si un usuario inicia sesión.
- Campo de búsqueda que redirige a una lista paginada con el resultado.
- Dropzone utilizado para modificar la imagen del usuario.

### Factorias AngularJS
- Factoría apiconnector utilizada para realizar comunicaciones con el servidor.
    - Diferentes métodos GET para realizar consultas al servidor con la posibilidad de añadir valores auxiliares o para conectar con api.
    - Métodos POST diferentes si es para un módulo o un componente.
- Modificación de elementos en el carrito mediante la factoria serviceCart.
- Factoria user token utilizada para guardar y recuperar el token del usuario.

### Login 
 - Social login realizado a través de Auth0 en AngularJS.

<br>

# Backend # Detalles técnicos

- Copia de la base de datos en el directorio doc.
- Separación de componentes y módulos en diferentes directorios.
- La aplicación inicia en index.php a traves del router.
- Servicio de correo electrónico a traves de la API mailgun.

### Router
- Metodo autoload para cargar las clases necesarias.
- Ficheros para indicar los componentes, módulos y funciones que tendrán.
- El token del usuario de actualiza al realizar alguna acciones.

### Componentes
- Logger
    - Clase para escribir un fichero de log con diferentes métodos (info, warning, error, y debug).
- Login
    - Clase user para realizar diferentes acciones con usuarios, (buscar por token, actualizar token...)
    - Libreria JWT utilizada para generar el token del usuario.
- Response 
    - Clase para generar respuestas genéricas al equipo cliente.
- Search
    - Busqueda genérica de artículos.

### Módulos
- Acceso y tratamiento de datos a través de BLL y DAO. 
- Excepciones con mensajes adaptables en caso de no existir algún método necesario.
- Login
    - Sistema de correo para activar cuentas y recuperar contraseñas.
- Cart
    - Funcionalidad a través de $_SESSION y base de datos.
    - Al iniciar sesion se comprueba si el usuario tiene datos del carrito guardados en base de datos.

<br>

<img src="doc/img/phpdoc.png" alt="PHP doc" width="750"/>

# Screenshots

<img src="doc/img/capturaCliente.png" alt="Vista cliente" width="750"/>
    
Vista cliente (order)

<img src="doc/img/capturaAdmin.png" alt="Vista admin" width="750"/>

Vista admin (crud)

<br>

# Otras tecnologías web
 - [JWT](https://jwt.io/)
 - [Toastr](http://embed.plnkr.co/YVfYwV/)
 - [UI Bootstrap](https://angular-ui.github.io/bootstrap/)
 - [Google Books API](https://play.google.com/books)
 - [Font Awesome](https://fontawesome.com/)