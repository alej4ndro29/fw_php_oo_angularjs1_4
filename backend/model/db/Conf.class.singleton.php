<?php
    class Conf {
        private $conf;

        private $_user;
        private $_passwd;
        private $_host;
        private $_db;

        static $_instance;
        
        public function __construct() {
            $conf = parse_ini_file("config.ini");

            $this->_user    = $conf['user'];
            $this->_passwd  = $conf['passwd'];
            $this->_host    = $conf['host'];
            $this->_db      = $conf['db'];
        }

        public static function getInstance() {
            if (!(self::$_instance instanceof self))
                self::$_instance = new self();
            return self::$_instance;
        }

        public function __get($property) {
            if (property_exists($this, $property)) {
                return $this->$property;
            }
        }

    }
    