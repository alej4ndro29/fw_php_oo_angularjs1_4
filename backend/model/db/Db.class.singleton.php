<?php
    class Db {
        private $server;
        private $user;
        private $passwd;
        private $database;
        private $sqli;
        private $stmt;
        private $array;

        static $_instance;

        private function __construct() {
            $this->getConnectionInfo();
        }

        public static function getInstance() {
            if (!(self::$_instance instanceof self))
                self::$_instance = new self();
            return self::$_instance;
        }


        private function getConnectionInfo() {
            require_once 'Conf.class.singleton.php';
            $conf = Conf::getInstance();

            $this->user = $conf->_user;
            $this->passwd = $conf->_passwd;
            $this->server = $conf->_host;
            $this->database = $conf->_db;

        }
        /**
         * Establish connection to database
         */
        private function connect() {
            $this->sqli = new mysqli($this->server, $this->user, $this->passwd);
            $this->sqli->select_db($this->database);
        }

        /**
         * Execute query on db
         * @param String $sql sentence to be executed
         * @return sqli object
         */
        public function createQuery($sql) {
            $this->connect();
            $this->stmt = $this->sqli->query($sql);
            $this->disconnect();
            return $this->stmt;
        }

        /**
         * List sqli object
         * @param sqli_object $stmt Object to be list
         * @return Array list element.
         */
        public function listQuery($stmt) {
            $this->array = array();

            foreach ($stmt as $key) {
                array_push($this->array, $key);
            }
            
            return $this->array;
        }

        /**
         * Get first element of query
         * @param sqli_object $stmt Object to get info
         * @return Array first element of query
         */
        public function listOne($stmt) {
            return mysqli_fetch_assoc($stmt);
        }

        /**
         * Disconnect from db
         */
        public function disconnect() {
            $this->sqli->close();
        }
        
    }
    