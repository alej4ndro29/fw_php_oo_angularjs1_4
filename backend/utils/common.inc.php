<?php

    /**
     * Funtion to execute model method
     * 
     * @param String $model_path Path of model
     * @param String $model_name Name of model
     * @param String $functionType (get / post) Get executes listQuery, post not
     * @param String $functionName Name of BLL funtion to execute
     * @param String/Array $arrArgument (optional) Params to bll
     * @throws Exception Not exists bll function
     * @throws Exception Not exists model file
     * @return Object Value of that bll function returns
     */
    function loadModel($model_path, $model_name, $functionType, $functionName, $arrArgument = ''){
        $model = $model_path . $model_name . '.class.singleton.php';

        if (file_exists($model)) {
            include_once($model);
        
            $modelClass = $model_name;

            if (!method_exists($modelClass, $functionType)){
                throw new Exception('Not exists model function -> '. $functionType);
            } else {
                $obj = $modelClass::getInstance();
                return $obj->$functionType($functionName, $arrArgument);
            }
            
        } else {
            throw new Exception('Not exists model file -> '. $model);
        }
    }
