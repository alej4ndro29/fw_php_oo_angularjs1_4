<?php
/**
 * Class for writte log file
 */
class Logger {

    const FILE_PATH = 'components/logger/log/log.log';
    static $_instance;

    function __construct() {}

    public static function getInstance() {
        if (!(self::$_instance instanceof self))
            self::$_instance = new self();
        return self::$_instance;
    }

    /**
     * @param String $message Message log for writte info log
     */
    public static function info($message) {
        $message = '[INFO] ' . date('d/m/Y H:i:s') . ' ' . $message . "\n";
        file_put_contents(self::FILE_PATH, $message, FILE_APPEND | LOCK_EX);
    }

     /**
     * @param String $message Message log for writte warning log
     */
    public static function warning($message) {
        $message = '[WARNING] ' . date('d/m/Y H:i:s') . ' ' . $message . "\n";
        file_put_contents(self::FILE_PATH, $message, FILE_APPEND | LOCK_EX);
    }

     /**
     * @param String $message Message log for writte error log
     */
    public static function error($message) {
        $message = '[ERROR] ' . date('d/m/Y H:i:s') . ' ' . $message . "\n";
        file_put_contents(self::FILE_PATH, $message, FILE_APPEND | LOCK_EX);
    }

     /**
     * @param String $message Message log for writte debug log
     */
    public static function debug($message) {
        self::info(json_encode($message));
    }
}
