<?php
    /**
     * Class for search db Resources
     */
    class search {
        const PATH_MODEL = __DIR__ . '/model/model/';
        static $_instance;

        function __construct() {}

        public static function getInstance() {
            if (!(self::$_instance instanceof self))
                self::$_instance = new self();
            return self::$_instance;
        }

        public function __get($property) {
            if (property_exists($this, $property)) {
                return $this->$property;
            }
        }

        /**
         * Function to search with limit
         */
        public function search() {
            try {
                if (sizeof($_POST) == 0) {
                    throw new Exception('Not data included');
                }
                echo json_encode(loadModel(self::PATH_MODEL, 'search_model', 'get', 'search'));
            } catch (Exception $e) {
                header('HTTP/1.0 400 Error Processing Request');
            }
        }

        /**
         * Function to search with param
         */
        public function searchParam() {
            try {
                if (!isset($_POST)) {
                    throw new Exception();
                }
                echo json_encode(loadModel(self::PATH_MODEL, 'search_model', 'get', 'searchParam'));
            } catch (Exception $e) {
                header('HTTP/1.0 400 Error Processing Request');
            }
        }

        /**
         * Function to search all
         */
        public function searchAll() {
            try {
                if (!isset($_POST)) {
                    throw new Exception();
                }
                echo json_encode(loadModel(self::PATH_MODEL, 'search_model', 'get', 'searchAll'));
            } catch (Exception $e) {
                header('HTTP/1.0 400 Error Processing Request');
            }
        }

    }
    