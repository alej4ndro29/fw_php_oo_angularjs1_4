<?php
    require 'jwt/JWT.php';
    use \Firebase\JWT\JWT;

    /**
     * Class for manage users
     */
    class User {

        const LOGIN_PATH = APP_PATH . '/modules/login/model/model/';

        private $id;
        private $nick;
        private $email;
        private $avatar;
        private $type;
        

        function __construct($data) {
            $this->id = $data['id'];
            $this->nick = $data['nickname'];
            $this->email = $data['email'];
            $this->avatar = $data['avatar'];
            $this->type = $data['typeUser'];
        }

        public function __get($property) {
            if (property_exists($this, $property)) {
                return $this->$property;
            }
        }

        public function __set($property, $value) {
            if (property_exists($this, $property)) {
                $this->$property = $value;
            }
        }

        public function isAdmin() {
            if ($this->type == 'admin') {
                return true;
            } else {
                return false;
            }
            
        }

        /**
         * Update token to user
         * 
         * @param String $currentToken User token to be replaced
         * @return String new user token
         */
        public function updateAppToken($currentToken) {

            $user = self::getUserByToken($currentToken);

            $newToken = self::generateAppToken($user['nickname']);
            
            $data = array(
                'oldToken' => $currentToken,
                'newToken' => $newToken
            );
            loadModel(self::LOGIN_PATH, 'login_model', 'post', 'setNewToken', $data);

            return $newToken;
        }

        /**
         * Generate token
         * @return String token.
         */
        public function generateAppToken($tokenApp = null) {
            $key = $_SESSION['TK_KEY'];
            
            $token = array(
                'id0' => uniqid(),
                'id1' => uniqid()
            );

            if ($tokenApp != null) {
                $token['user'] = $tokenApp;
            }

            $jwt = JWT::encode($token, $key, 'HS256');

            return $jwt;
        }

        /**
         * Get a user with token
         * @param String $token Token to find user
         * @return Array user
         */
        public function getUserByToken($token) {
            $data = array(
                'userToken' => $token
            );

            return loadModel(self::LOGIN_PATH, 'login_model', 'post', 'getUser', $data);
        }

        /**
         * Generate a token for email
         * 
         * @param String $seed Seed to generate token
         * @return String token
         */
        public function generateEmailToken($seed) {
            return md5(uniqid($seed));
        }

        /**
         * Generate email url
         * 
         * @param String $token
         * @return String Url for activate account
         */
        public function generateEmailUrl($token) {
            return SITE_NAME . '#/login/activate/'.$token;
        }

        /**
         * Generate url for recover password
         * @param String $token User token for generate url
         * @return String Url for recover password
         */
        public function generateRecpasswdUrk($token) {
            return SITE_NAME . '#/login/recover/' . $token;
        }

    }