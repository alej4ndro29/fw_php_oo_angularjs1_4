<?php
    /**
     * Class for generate generic responses to clien
     */
    class Response {
        static $_instance;
        
        function __construct() {}

        public static function getInstance() {
            if (!(self::$_instance instanceof self))
                self::$_instance = new self();
            return self::$_instance;
        }

        /**
         * Send generic correct response 
         * @param String $message optional param to send to client
         */
        public function ok($message = null) {
            $response['correct'] = true;
            
            if ($message != null) {
                $response['message'] = $message;
            }

            echo json_encode($response);

        }

        /**
         * Send generic incorrect response 
         * @param String $message optional param to send to client
         */
        public function nok($message = null) {
            $response['correct'] = false;

            if ($message != null) {
                $response['message'] = $message;
            }

            echo json_encode($response);
        }


    }
