<?php
    class homePage {
    
        const PATH_MODEL = __DIR__ . '/model/model/';
        static $_instance;

        function __construct() {}

        public static function getInstance() {
            if (!(self::$_instance instanceof self))
                self::$_instance = new self();
            return self::$_instance;
        }

        public function __get($property) {
            if (property_exists($this, $property)) {
                return $this->$property;
            }
        }

        public function search() {
            try {
                echo json_encode(loadModel(self::PATH_MODEL, 'homePage_model', 'get', 'search'));
            } catch (Exception $e) {
                echo $e->getMessage();
            }

        }

        public function autocomplete() {
            try {
                echo json_encode (loadModel(self::PATH_MODEL, 'homePage_model', 'get', 'autocomplete'));
            } catch (Exception $e) {
                echo $e->getMessage();
            }

        }

        public function getSuggestions() {
            try {
                echo json_encode(loadModel(self::PATH_MODEL, 'homePage_model', 'get', 'suggestions'));
            } catch (Exception $e) {
                echo $e->getMessage();
            }
        }

        public function getItems() {
            try {
                echo json_encode(loadModel(self::PATH_MODEL, 'homePage_model', 'get', 'items'));
            } catch (Exception $e) {
                echo $e->getMessage();
            }
        }


    }