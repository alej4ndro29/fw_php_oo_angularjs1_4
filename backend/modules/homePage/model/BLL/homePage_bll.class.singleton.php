<?php
    class homePage_bll {
        private $dao;
        private $db;

        static $_instance;

        private function __construct() {
            $this->dao = homePage_dao::getInstance();
            $this->db = Db::getInstance();
        }

        public static function getInstance() {
            if (!(self::$_instance instanceof self)){
                self::$_instance = new self();
            }
            return self::$_instance;
        }
 
        private function getActiveUserID() {
            if (!isset($_SESSION['activeUser'])) {
                throw new Exception('User not logged');
            }
            $user = unserialize($_SESSION['activeUser']);
            return $user->getID();
        }

        private function executeDAO($functionName, $arrAguments = '') {
            if (method_exists($this->dao, $functionName)) {
                return $this->dao->$functionName($this->db, $arrAguments);
            } else {
                throw new Exception('Not exists DAO function -> '. $functionName);
            }
        }

        /////////GET FUNCTIONS//////////////

        public function suggestions($arrAguments) {
            return $this->executeDAO('daoGETsuggestions', $arrAguments);
        }


        public function search($arrAguments) {
                // 0|1          0|1             0|1
                // type         gen             input
                $selects = "";
                if ($_POST['typ'] == "") {
                    $selects = $selects . "0";
                } else {
                    $selects = $selects . "1";
                }

                if ($_POST['gen'] == "") {
                    $selects = $selects . "0";
                } else {
                    $selects = $selects . "1";
                }

                if ($_POST['inp'] == "") {
                    $selects = $selects . "0";
                } else {
                    $selects = $selects . "1";
                }

                switch ($selects) {
                    case '000':
                        die();
                        break;
                    case '001':
                        return $this->executeDAO('daoGETs_001');
                        break;
                    case '010':
                        return $this->executeDAO('daoGETs_010');
                        break;
                    case '011':
                        return $this->executeDAO('daoGETs_011');
                        break;
                    case '100':
                        return $this->executeDAO('daoGETs_100');
                        break;
                    case '101':
                        return $this->executeDAO('daoGETs_101');
                        break;
                    case '110':
                        return $this->executeDAO('daoGETs_110');
                        break;
                    case '111':
                        return $this->executeDAO('daoGETs_111');
                        break;
                }

        }


        public function autocomplete($arrAguments) {
            return $this->executeDAO('daoGETautocomplete');
        }

        public function items() {
            return $this->executeDAO('daoGETitems');
        }

        /////////////POST FUNCTIONS//////////////////////


    }
    