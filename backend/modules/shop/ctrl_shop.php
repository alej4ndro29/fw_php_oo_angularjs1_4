<?php
    class shop {

        const PATH_MODEL = __DIR__ . '/model/model/';
        static $_instance;

        function __construct() {}

        public static function getInstance() {
            if (!(self::$_instance instanceof self))
                self::$_instance = new self();
            return self::$_instance;
        }

        public function __get($property) {
            if (property_exists($this, $property)) {
                return $this->$property;
            }
        }

        public function getItem() {
            try {
                echo json_encode(loadModel(self::PATH_MODEL, 'shop_model', 'get', 'item'));
            } catch (Exception $e) {
                echo $e->getMessage();
            }
        }

    }