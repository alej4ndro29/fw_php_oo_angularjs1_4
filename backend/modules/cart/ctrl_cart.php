<?php
class cart {
        const PATH_MODEL = __DIR__ . '/model/model/';
        static $_instance;

        function __construct() {}

        public static function getInstance() {
            if (!(self::$_instance instanceof self))
                self::$_instance = new self();
            return self::$_instance;
        }

        public function __get($property) {
            if (property_exists($this, $property)) {
                return $this->$property;
            }
        }

        public function getList() {
            echo json_encode($_SESSION['cart']);
        }

        public function addToCart() {
            try {
                loadModel(self::PATH_MODEL, 'cart_model', 'post', 'addToCart', $_POST);
            } catch (Exception $e) {
                echo $e->getMessage();
            }
            
        }

        public function getElementList() {
            try {
                loadModel(self::PATH_MODEL, 'cart_model', 'post', 'getElementList', $_GET['aux']);
            } catch (Exception $e) {
                echo $e->getMessage();
            }
        
        }

        public function minusOne() {
            try {
                echo loadModel(self::PATH_MODEL, 'cart_model', 'post', 'minusOne', $_POST);
            } catch (Exception $e) {
                header('HTTP/1.0 500 Bad error');
            }
        }

        public function addOne() {
            try {
                echo loadModel(self::PATH_MODEL, 'cart_model', 'post', 'addOne', $_POST);
            } catch (Exception $e) {
                header('HTTP/1.0 500 Bad error');
            }
        }

        public function removeItem() {
            try {
                echo loadModel(self::PATH_MODEL, 'cart_model', 'post', 'removeItem', $_POST);
            } catch (Exception $e) {
                header('HTTP/1.0 500 Bad error');
            }
        }

        public function completeOrder() {
            try {
                loadModel(self::PATH_MODEL, 'cart_model', 'post', 'completeOrder', $_POST);
            } catch (Exception $e) {
                header('HTTP/1.0 500 Bad error');
            }
        }
}