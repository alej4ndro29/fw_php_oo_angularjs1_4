<?php
    class cart_bll {
        private $dao;
        private $db;

        static $_instance;

        private function __construct() {
            $this->dao = cart_dao::getInstance();
            $this->db = Db::getInstance();
        }

        public static function getInstance() {
            if (!(self::$_instance instanceof self)){
                self::$_instance = new self();
            }
            return self::$_instance;
        }

        private function executeDAO($functionName, $arrAguments = '') {
            if (method_exists($this->dao, $functionName)) {
                return $this->dao->$functionName($this->db, $arrAguments);
            } else {
                throw new Exception('Not exists DAO function -> '. $functionName);
            }
        }

        /////////GET FUNCTIONS//////////////

        /////////////POST FUNCTIONS//////////////////////
        
        public function getElementList($id) {
            $info = array();
            $result = $this->executeDAO('searchItem', $id);
            $result = mysqli_fetch_assoc($result);

            for ($i = 0; $i < count($_SESSION['cart']); $i++) {
                if ($_SESSION['cart'][$i]['productID'] == $id) {
                    // echo ' dentro if ';
                    $quantity = $_SESSION['cart'][$i]['quantity'];
                }
            }

            $info['name'] = $result['nameResource'];
            $info['price'] = $result['price'];
            $info['quantity'] = $quantity;
            $info['id'] = $id;
            echo json_encode($info);
            
        }

        public function addToCart($data) {
            $result = $this->executeDAO('searchItem', $data['item']);
            
            if ($result->num_rows != 1){
                throw new Exception('error-item');
            }
            
            if(isset($data['token'])) {
                $token = $data['token'];
                $user = User::getUserByToken($token);
            }

            $result = mysqli_fetch_assoc($result);
                    
            $ok = true;
            for ($i=0; $i < count($_SESSION['cart']); $i++) {
                // Aumentar si ya está en el carrito
                if ($_SESSION['cart'][$i]['productID'] == $_POST['item']) {
                    $_SESSION['cart'][$i]['quantity']++;
                    $ok = false;

                    if (isset($user)) {
                        $datadao['userID'] = $user['id'];
                        $datadao['resourceID'] = $data['item'];
                        // Logger::debug($datadao);
                        $this->executeDAO('addOne', $datadao);
                    }
                }
            }

            if ($ok) {
                array_push($_SESSION['cart'], array(
                    'productID' =>  $result['IDResource'],
                    'quantity' => 1
                ));

                // REFACTOR                
                if(isset($user)) {
                        $datadao['userID'] = $user['id'];
                        $datadao['resourceID'] = $data['item'];
                        $datadao['quatity'] = 1;
                        // Logger::debug($datadao);
                        $this->executeDAO('createCart', $datadao);
                }
            }
        }

        public function minusOne($data) {
            if(isset($data['token'])) {
                $token = $data['token'];
                $user = User::getUserByToken($token);
            }

            $ok = false;
            for ($i = 0; $i < count($_SESSION['cart']); $i++) {
                if ($_SESSION['cart'][$i]['productID'] == $_POST['item']) {
                    if ($_SESSION['cart'][$i]['quantity'] > 1) {
                        $_SESSION['cart'][$i]['quantity']--;

                        if (isset($user)) {    
                            $datadao['userID'] = $user['id'];
                            $datadao['resourceID'] = $data['item'];
                            $this->executeDAO('minusOne', $datadao);
                        }
                    } else {
                        // ELIMINAR EL PRODUCTO SI SOLO HAY UN PRODUCTO
                        unset($_SESSION['cart'][$i]);
                        $_SESSION['cart'] = array_values($_SESSION['cart']);
                        
                        if (isset($user)) {
                            $datadao['userID'] = $user['id'];
                            $datadao['resourceID'] = $data['item'];
                            $this->executeDAO('removeItem', $datadao);
                        }
                    }
                    $ok = true;
                }
            }
            return $ok;
        }

        public function addOne($data) {
            if(isset($data['token'])) {
                $token = $data['token'];
                $user = User::getUserByToken($token);
            }

            $ok = false;
            for ($i = 0; $i < count($_SESSION['cart']); $i++) {
                if ($_SESSION['cart'][$i]['productID'] == $_POST['item']) {
                    $_SESSION['cart'][$i]['quantity']++;
                    $ok = true;

                    if (isset($user)) {
                        $datadao['userID'] = $user['id'];
                        $datadao['resourceID'] = $data['item'];
                        $this->executeDAO('addOne', $datadao);
                    }
                }
            }
            return $ok;
        }

        public function removeItem($data) {
            if(isset($data['token'])) {
                $token = $data['token'];
                $user = User::getUserByToken($token);
            }

            $ok=false;
            for ($i=0; $i < count($_SESSION['cart']); $i++) { 
                if ($_SESSION['cart'][$i]['productID'] == $_POST['item']) {
                    unset($_SESSION['cart'][$i]);
                    $_SESSION['cart']=array_values($_SESSION['cart']);
                    $ok=true;

                    if (isset($user)) {
                        $datadao['userID'] = $user['id'];
                        $datadao['resourceID'] = $data['item'];
                        $this->executeDAO('removeItem', $datadao);
                    }
                }
            }
            return $ok;
        }

        private function getQuantity($id) {
            foreach ($_SESSION['cart'] as $key) {
                if($key['productID'] == $id) {
                    return $key['quantity'];
                }
            }
            return '';
        }

        public function completeOrder($data) {
            if (sizeof($_SESSION['cart']) == 0) {
                throw new Exception('Empty cart');
            }

            $token = $data['userToken'];
            $user = User::getUserByToken($token);

            if ($user == null) {
                throw new Exception('Invalid user');
            }

            $purchaseId =  $user['id'] . '_' . date('d-m-Y_H:i:s');

            // Create dao contition
            $condition = '(';
            foreach ($_SESSION['cart'] as $key) {
                if ($condition != '(') {
                    $condition .= ', ';
                }
                $condition .= $key['productID'];
            }
            $condition .= ')';

            $dataDao = array(
                'condition' => $condition
            );
            $articles = $this->executeDAO('getItemsCondition', $dataDao);
            
            $toHistory = array();
        
            foreach ($articles as $key) {
                $current = array(
                    'userID' => $user['id'],
                    'resourceID' => $key['idResource'],
                    'price' => $key['price'],
                    'quantity' => $this->getQuantity($key['idResource']),
                    'purchaseID' => $purchaseId
                );

                array_push($toHistory, $current);

            }

            $this->executeDAO('toHistory', $toHistory);

            $_SESSION['cart'] = null;
            $_SESSION['cart'] = array();

            $deleteCart['userID'] = $user['id'];
            $this->executeDAO('deleteCart', $deleteCart);
        }
    }
    