<?php

include 'model/utils/regex.php';
include 'model/utils/gravatar.php';

class login {

    const PATH_MODEL = __DIR__ . '/model/model/';
    static $_instance;

    function __construct() {}

    public static function getInstance() {
        if (!(self::$_instance instanceof self))
            self::$_instance = new self();
        return self::$_instance;
    }

    public function __get($property) {
        if (property_exists($this, $property)) {
            return $this->$property;
        }
    }

    public function socialLogin() {
        try {
            echo json_encode(loadModel(self::PATH_MODEL, 'login_model', 'post', 'socialLogin', $_POST));
        } catch (Exception $e) {
            Logger::warning('Error social login ' . $e->getMessage());
            Response::getInstance()->nok($e->getMessage());
        }
    }

    public function register() {
        try {
            if (!regFormatCheck()) {
                throw new Exception('Data bad format');
            }

            loadModel(self::PATH_MODEL, 'login_model', 'post', 'registerLocalUser', $_POST);
            Response::getInstance()->ok();

        } catch (Exception $e) {
            switch ($e->getMessage()) {
                case 'Data bad format':
                    Response::getInstance()->nok($e->getMessage());
                    break;
                case 'userAlredyRegistered':
                    Response::getInstance()->nok($e->getMessage());
                    break;
                default:
                    error_log($e->getMessage());
                    Logger::getInstance()->warning("Error register user " . $e->getMessage());
                    header('HTTP/1.0 500 Server error');
                    break;
            }

        }
        
    }

    public function activate() {
        try {
            loadModel(self::PATH_MODEL, 'login_model', 'post', 'activeAccount');
            Response::getInstance()->ok();
        } catch (Exception $e) {
            Response::getInstance()->nok();
        }
    }

    public function login() {
        try {
            echo json_encode(loadModel(self::PATH_MODEL, 'login_model', 'post', 'loginLocalUser', $_POST));
        } catch (Exception $e) {
            Response::getInstance()->nok($e->getMessage());
        }

    }

    public function logout() {
        session_destroy();
    }

    public function getUserByToken() {
        try {
            echo json_encode(loadModel(self::PATH_MODEL, 'login_model', 'post', 'getUserByToken', $_POST));
        } catch (Exception $e) {
            Response::getInstance()->nok($e->getMessage());
        }

    }

    public function recPasswd() {
        try {
            loadModel(self::PATH_MODEL, 'login_model', 'post', 'recPasswd', $_POST);
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function setRecPasswd() {
        try {
            loadModel(self::PATH_MODEL, 'login_model', 'post', 'setRecPasswd', $_POST);
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

}
