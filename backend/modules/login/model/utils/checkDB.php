<?php
    include 'dao/utilsDAO.php';

    if (!isset($_SESSION)) {
        session_start();
    }

    function nickExistsCheck($nickToSearch) {
        $nick = findNick($nickToSearch);
        
        if($nick->num_rows == 0) {
            return true;
        } else {
            return false;
        }

    }

    function emailExistsCheck($emailToSearch) {
        $email = findEmail($emailToSearch);

        if($email->num_rows == 0){
            return true;
        } else {
            return false;
        }

    }

    function hasElementsOnCart($userID){
        $cart = getCartUser($userID);
        if ($cart->num_rows == 0) {
            //error_log(print_r($_SESSION['cart'],true));
            $cant = count($_SESSION['cart']);

            if ($cant != 0) {
                for ($i=0; $i < count($_SESSION['cart']); $i++) { 
                    $productID = $_SESSION['cart'][$i]['productID'];
                    $quantity = $_SESSION['cart'][$i]['quantity'];

                    createCart($userID, $productID, $quantity);
                }
            } 
        } else {
            $_SESSION['cart'] = null;
            $_SESSION['cart'] = array();
            foreach ($cart as $key) {
                array_push($_SESSION['cart'], array(
                    'productID' =>  $key['resourceID'],
                    'quantity' => $key['quantity']
                ));
            }
        }
    }
