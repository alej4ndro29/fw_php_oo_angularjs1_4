<?php

    function verifyNick($nick) {
        $regexp = '/^[a-zA-Z0-9]+$/';
        return preg_match($regexp, $nick);
    }

    function verifyEmail($email) {
        $regexp = '/^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i';
        return preg_match($regexp, $email);
    }

    function verifyPasswd($pass0, $pass1) {
        if ($pass0 == $pass1) {
            return true;
        } else {
            return false;
        }
    }

    function regFormatCheck() {        
        $nickOK = verifyNick($_POST['reg-nick']);
        $emailOK = verifyEmail($_POST['reg-email']);
        $passwdOK = verifyPasswd($_POST['reg-password'], $_POST['reg-password-rep']);
        
        if($nickOK && $emailOK && $passwdOK) {
           return true;
        } else {
            return false;
        }
    }