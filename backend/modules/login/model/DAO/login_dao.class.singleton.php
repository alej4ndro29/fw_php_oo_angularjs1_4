<?php
    class login_dao {
        static $_instance;

        private function __construct() {
        }

        public static function getInstance() {
            if(!(self::$_instance instanceof self)) {
                self::$_instance = new self();
            }
            return self::$_instance;
        }

        public function localUserExists($db, $nick) {
            $result = $db->createQuery("SELECT count(*) quantity
                                        FROM users
                                        WHERE id LIKE 'local%'
                                        AND nickname = '$nick';");
            
            $result = $db->listOne($result);
            
            if($result['quantity'] != 0) {
                return true;
            } else {
                return false;
            }
        }

        public function daoRegisterLocalUser($db, $userData) {
            $id         = $userData['id'];
            $nickName   = $userData['nickName'];
            $email      = $userData['email'];
            $passwd       = $userData['pass'];
            $emailToken = $userData['emailToken'];
            $avatar     = $userData['avatar'];

            $sql ="INSERT INTO users(
                    id,
                    nickname,
                    passwd,
                    email,
                    emailToken,
                    avatar
                    ) VALUES (
                    '$id',
                    '$nickName',
                    '$passwd',
                    '$email',
                    '$emailToken',
                    '$avatar'
                    );";

            return $db->createQuery($sql);
        }

        public function daoRegisterSocialUser($db, $userData) {
            $id = $userData['idTokenPayload']['sub'];
            $nickname = $userData['idTokenPayload']['nickname'];
            $avatar = $userData['idTokenPayload']['picture'];

            $sql = "INSERT INTO users
                    (
                    id,
                    active,
                    nickname,
                    avatar
                    )
                    VALUES
                    (
                    '$id',
                    '1',
                    '$nickname',
                    '$avatar'
                    );";

            $result =  $db->createQuery($sql);
            return $result;
        }

        public function daoUserTokenExist($db, $token) {
            $sql = "SELECT count(*) count, active isActive, email
                    FROM users
                    WHERE emailToken = '$token';";

            $result = $db->createQuery($sql);
            $result = $db->listOne($result);

            if($result['count']) {
                return $result;
            } else {
                return false;
            }

        }

        public function activeUser($db, $token) {
            $sql = "UPDATE users
                    SET active = '1'
                    WHERE emailToken = '$token';";
            
            $db->createQuery($sql);
            
        }

        public function socialUserIsRegistered($db, $userData) {
            $id = $userData['idTokenPayload']['sub'];
            $sql = "SELECT count(*) count
                    FROM users
                    WHERE id = '$id'";

            $result = $db->createQuery($sql);
            $result = $db->listOne($result);

            return $result['count'];
        }

        public function daoGetLocalUser($db, $userData) {
            $nickName = $userData['lg-nick'];
            $sql = "SELECT *
                    FROM users
                    WHERE id LIKE 'local%'
                    AND nickname = '$nickName';";

            $result = $db->createQuery($sql);
            $result = $db->listOne($result);

            return $result;
        }

        public function daoIsActiveUser($db, $userID) {
            $sql = "SELECT active
                    FROM users
                    WHERE id = '$userID';";

            $result =  $db->createQuery($sql);
            $result = $db->listOne($result);

            return $result['active'];
        }

        public function daoUpdateUserAppToken($db, $data) {
           $id = $data['id'];
           $token = $data['token'];

            $sql = "UPDATE users
                    SET
                    appToken = '$token'
                    WHERE id = '$id';";

            $db->createQuery($sql);

        }

        public function daoSetNewToken($db, $data) {
            $old = $data['oldToken'];
            $new = $data['newToken'];

            $sql = "UPDATE users
                    SET
                    appToken = '$new'
                    WHERE appToken = '$old';";

            $db->createQuery($sql);

        }
        
        public function daoGetUserByTokenMin($db, $data) {
            $token = $data['userToken'];

            $sql = "SELECT nickname, avatar, typeUser
                    FROM users
                    WHERE appToken = '$token';";

            $result = $db->createQuery($sql);
            $result = $db->listOne($result);

            return $result;

        }

        public function daoGetUserByToken($db, $data) {
            $token = $data['userToken'];

            $sql = "SELECT *
                    FROM users
                    WHERE appToken = '$token';";

            $result = $db->createQuery($sql);
            $result = $db->listOne($result);

            return $result;

        }

        public function getLocalByMail($db, $data) {
            $email = $data['email'];
            $sql = "SELECT *
                    FROM users
                    WHERE id LIKE 'local|%'
                    AND email = '$email';";

            $result = $db->createQuery($sql);
            return $db->listOne($result);
        }

        public function setRecPass($db, $data) {
            $passwd = $data['passwd'];
            $token  = $data['token'];

            $sql = "UPDATE users
                    SET
                    passwd = '$passwd'
                    WHERE emailToken = '$token';";

            $db->createQuery($sql);
        }

        // Login cart actions

        public function getCartUser($db, $data) {
            $userID = $data['userID'];

            $sql = "SELECT resourceID, quantity
                    FROM cart
                    WHERE userID = '$userID';";

            $result = $db->createQuery($sql);
            $result = $db->listQuery($result);
            return $result;  
        }
    
        public function createCart($db, $data) {
            $userID    = $data['userID'];
            $productID = $data['productID'];
            $quantity  = $data['quatity'];

            $sql = "INSERT INTO cart
                        (userID,
                        resourceID,
                        quantity)
                    VALUES (
                        '$userID',
                        '$productID',
                        '$quantity'
                    );";
            $db->createQuery($sql);
        }

    }
    
