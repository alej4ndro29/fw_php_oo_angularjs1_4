<?php
    class login_bll {
        private $dao;
        private $db;

        static $_instance;

        private function __construct() {
            $this->dao = login_dao::getInstance();
            $this->db = Db::getInstance();
        }

        public static function getInstance() {
            if (!(self::$_instance instanceof self)){
                self::$_instance = new self();
            }
            return self::$_instance;
        }

        private function executeDAO($functionName, $arrAguments = '') {
            if (method_exists($this->dao, $functionName)) {
                return $this->dao->$functionName($this->db, $arrAguments);
            } else {
                throw new Exception('Not exists DAO function -> '. $functionName);
            }
        }

        private function hasElementsOnCart($userID) {
            $data['userID'] = $userID;
            $cart = $this->executeDAO('getCartUser', $data);

            // db check
            if (sizeof($cart) == 0) { // 
                $cant = count($_SESSION['cart']);

                if ($cant != 0) {
                    for ($i=0; $i < count($_SESSION['cart']); $i++) {
                        $data['productID'] = $_SESSION['cart'][$i]['productID'];
                        $data['quatity']   = $_SESSION['cart'][$i]['quantity'];
                        
                        $this->executeDAO('createCart', $data);    
                    }
                    // Logger::info("create cart in db");
                } else {
                    // Logger::info("not cart in session");
                }
            } else {
                $_SESSION['cart'] = null;
                $_SESSION['cart'] = array();
                foreach ($cart as $key) {
                    array_push($_SESSION['cart'], array(
                        'productID' =>  $key['resourceID'],
                        'quantity' => $key['quantity']
                    ));
                }
                $cartLog = json_encode($_SESSION['cart']);
                // Logger::info("load from bd -> " . $cartLog);
            }
        }

        /////////GET FUNCTIONS//////////////

        /////////////POST FUNCTIONS//////////////////////

        public function registerLocalUser($arrAguments) {
            $nick = $arrAguments['reg-nick'];
            $email = $arrAguments['reg-email'];
            $pass = password_hash($arrAguments['reg-password'], PASSWORD_DEFAULT);

            // Check
            $isUserAlredyRegistered = $this->executeDAO('localUserExists', $nick);

            if($isUserAlredyRegistered) {
               throw new Exception("userAlredyRegistered");
            }

            // Generate
            $emailToken = User::generateEmailToken($nick . microtime());
            $emailUrl   = User::generateEmailUrl($emailToken);

            // Gravatar
            $gravatar = get_gravatar($email);

            // Add to db
            $dataTodb = array(
                'id'         => 'local|'.$nick,
                'nickName'   => $nick,
                'email'      => $email,
                'pass'       => $pass,
                'emailToken' => $emailToken,
                'avatar'   => $gravatar
            );
            $this->executeDAO('daoRegisterLocalUser', $dataTodb);
            
            // Mail
            $from    = APP_NAME;
            $subject = 'Registration mail';

            $html    = $nick.' welcome!<br/>';
            $html   .= '<b>Please active your account</b><br/>';
            $html   .= 'Thanks for signing up, remember to <a href="'.$emailUrl.'">activate</a> your account.';

            sendMail($from, $email, $subject, $html);
        }

        public function activeAccount() {
            $token = $_POST['token'];
            
            // Load user
            $userInfo = $this->executeDAO('daoUserTokenExist', $token);

            // Check if exists
            if(!$userInfo['count']) {
                throw new Exception("tokenNotFound");
            }

            // Check if is active
            if ($userInfo['isActive']) {
                throw new Exception("userAlredyActive");
            } else {
                $this->executeDAO('activeUser', $token);
                
                $from = APP_NAME;
                $to   = $userInfo['email'];
                $subject = 'Your count has been activated';
                $html = 'Your count has been activated';

                sendMail($from, $to, $subject, $html);
            }

        }

        public function socialLogin($userData) {
            $isUserAlredyRegistered = $this->executeDAO('socialUserIsRegistered', $userData);

            // Registrar si es nuevo
            if(!$isUserAlredyRegistered) {
                // Logger::info('Register social user');
                $this->executeDAO('daoRegisterSocialUser', $userData);
            } 
            
            // Loguear
            $id = $userData['idTokenPayload']['sub'];
            $token = User::generateAppToken();

            $data = array(
                'id' => $id,
                'token' => $token
            );
            $this->executeDAO('daoUpdateUserAppToken', $data);

            $this->hasElementsOnCart($id);

            $response['userToken'] = $token;
            return $response;
            
        }

        public function loginLocalUser($userData) {
            $userDB = $this->executeDAO('daoGetLocalUser', $userData);
            
            if(!isset($userDB['id'])) {
                throw new Exception('User not exists');
            }

            if (!password_verify($userData['lg-password'], $userDB['passwd'])) {
                throw new Exception('Invalid password');
            }

            $isActiveUser = $this->executeDAO('daoIsActiveUser', $userDB['id']);
            if(!$isActiveUser) {
                $emailUrl   = User::generateEmailUrl($userDB['emailToken']);

                $from    = APP_NAME;
                $subject = 'Registration mail';

                $html    = $userDB['nickname'].' welcome!<br/>';
                $html   .= '<b>Please active your account</b><br/>';
                $html   .= 'Thanks for signing up, remember to <a href="'.$emailUrl.'">activate</a> your account.';

                sendMail($from, $userDB['email'], $subject, $html);

                throw new Exception('Check your email for enable your account');
            }

            $token =  User::generateAppToken();

            $data = array(
                'id'    => $userDB['id'],
                'token' => $token
            );
            $this->executeDAO('daoUpdateUserAppToken', $data);
            
            $this->hasElementsOnCart($userDB['id']);

            $response = array(
                'correct' => true,
                'token' => $token
            );

            return $response;

        }

        public function getUserByToken($data) {
            $currentToken = $data['userToken'];
            $response = $this->executeDAO('daoGetUserByTokenMin', $data);

            $response['token'] = User::updateAppToken($currentToken);

            return $response;
        }

        public function setNewToken($data) {
            $this->executeDAO('daoSetNewToken',$data);
        }

        public function getUser($data) {
            return $this->executeDAO('daoGetUserByToken', $data);
        }

        public function recPasswd($data) {
            $user = $this->executeDAO('getLocalByMail', $data);
            if($user == null) {
                throw new Exception('Not exists');
            }

            $url = User::generateRecpasswdUrk($user['emailToken']);

            $from = APP_NAME;
            $to   = $user['email'];
            $subject = 'Recover password';
            $html = '<a href="' . $url . '">Recover password</a>';

            sendMail($from, $to, $subject, $html);

        }

        public function setRecPasswd($data) {
            $encPass = password_hash($data['passwd'], PASSWORD_DEFAULT);

            $info = array(
                'passwd' => $encPass,
                'token'  => $data['token']
            );

            $this->executeDAO('setRecPass', $info);
        }

    }
    