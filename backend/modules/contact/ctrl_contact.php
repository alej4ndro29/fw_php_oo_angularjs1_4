<?php
    class contact {
    
        const PATH_MODEL = __DIR__ . '/model/model/';
        static $_instance;

        function __construct() {}

        public static function getInstance() {
            if (!(self::$_instance instanceof self))
                self::$_instance = new self();
            return self::$_instance;
        }

        public function __get($property) {
            if (property_exists($this, $property)) {
                return $this->$property;
            }
        }

        public function sendMail() {
            try {
                // Send mail to admin
                $from      = $_POST['name'];
                $userEmail = $_POST['mail'];

                $to      = $_SESSION['ADMIN_EMAIL'];
                $subject = 'New message from <' . $userEmail . '>';
                $html    = $_POST['message'];

                sendMail($from, $to, $subject, $html);

                // Send mail to user
                $to      = $userEmail;
                $subject = 'Copy of your message.';
                sendMail($from, $to, $subject, $html);

                Response::ok();

            } catch (Exception $e) {
                header('HTTP/1.0 400 Bad error');
            }
        }

    }
