<?php
class order {
    
    const PATH_MODEL = __DIR__ . '/model/model/';
    static $_instance;

    function __construct() {}

    public static function getInstance() {
        if (!(self::$_instance instanceof self))
            self::$_instance = new self();
        return self::$_instance;
    }

    public function __get($property) {
        if (property_exists($this, $property)) {
            return $this->$property;
        }
    }

    public function getData() {
        try {
            echo json_encode(loadModel(self::PATH_MODEL, 'order_model', 'get', 'getData'));
        } catch (Exception $e) {
            echo $e->getMessage();
            // header('HTTP/1.0 500 Bad error');
        }
    }

    public function newLike() {
        try {
            echo json_encode(loadModel(self::PATH_MODEL, 'order_model', 'post', 'newLike', $_POST));
        } catch (Exception $e) {
            header('HTTP/1.0 500 Bad error');
        }

    }

}
    