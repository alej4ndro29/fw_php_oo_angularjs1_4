<?php 
    class profile {
        const PATH_MODEL = __DIR__ . '/model/model/';
        static $_instance;

        function __construct() {}

        public static function getInstance() {
            if (!(self::$_instance instanceof self))
                self::$_instance = new self();
            return self::$_instance;
        }

        public function __get($property) {
            if (property_exists($this, $property)) {
                return $this->$property;
            }
        }

        public function loadProvinces() {
            try {
                echo json_encode(loadModel(self::PATH_MODEL, 'profile_model', 'get', 'provinces'));
            } catch (Exception $e) {
                header('HTTP/1.0 500 Bad error');
            }
        }

        public function loadTown() {
            try {
                echo json_encode(loadModel(self::PATH_MODEL, 'profile_model', 'get', 'town', $_GET['aux']));
            } catch (Exception $e) {
                header('HTTP/1.0 500 Bad error');
            }
        }

        public function loadUserInfo() {
            try {
                loadModel(self::PATH_MODEL, 'profile_model', 'post', 'userInfo', $_POST);
            } catch (Exception $e) {
                error_log($e->getMessage());
            }
        }

        public function loadLikes() {
            try {
                echo json_encode(loadModel(self::PATH_MODEL, 'profile_model', 'get', 'likes', $_POST));
            } catch (Exception $e) {
                echo ($e->getMessage());
            }
        }

        public function loadPurchases() {
            try {
                echo json_encode(loadModel(self::PATH_MODEL, 'profile_model', 'get', 'purchases', $_POST));
            } catch (Exception $e) {
                echo ($e->getMessage());
            }   
        }

        public function updateProfile() {
            try {
                loadModel(self::PATH_MODEL, 'profile_model', 'post', 'updateProfile', $_POST);
            } catch (Exception $e) {
                Response::nok($e->getMessage());
            }
        }

        public function uploadAvatar() {
            try {
                loadModel(self::PATH_MODEL, 'profile_model', 'post', 'uploadAvatar');
            } catch (Exception $e) {
                Response::nok($e->getMessage());
            }
        }

        public function setAvatar() {
            try {
                loadModel(self::PATH_MODEL, 'profile_model', 'post', 'setAvatar', $_POST);
            } catch (Exception $e) {
                Response::nok($e->getMessage());
            }
        }

        public function deleteAvatar() {
            try {
                loadModel(self::PATH_MODEL, 'profile_model', 'post', 'deleteAvatar', $_POST);
            } catch (Exception $e) {
                Response::nok($e->getMessage());
            }
        }
    }
    