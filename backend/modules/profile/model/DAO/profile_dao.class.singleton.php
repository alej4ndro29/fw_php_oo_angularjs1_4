<?php
    class profile_dao {
        static $_instance;

        private function __construct() {
        }

        public static function getInstance() {
            if(!(self::$_instance instanceof self)) {
                self::$_instance = new self();
            }
            return self::$_instance;
        }


        public function daoGETprovinces($db, $arrAguments) {
            $json = array();
            $tmp = array();

            $provincias = simplexml_load_file($_SERVER['DOCUMENT_ROOT'].'/backend/resources/country/provinciasypoblaciones.xml');
            $result = $provincias->xpath("/lista/provincia/nombre | /lista/provincia/@id");
            for ($i=0; $i<count($result); $i+=2) {
                $e=$i+1;
                $provincia=$result[$e];

                $tmp = array(
                'id' => (string) $result[$i], 'nombre' => (string) $provincia
                );
                array_push($json, $tmp);
            }
            
            // print_r($json);
            // die;

            return $json;
        }

        public function daoGETtown($db, $arrArgument) {
            $json = array();
            $tmp = array();

            $filter = (string)$arrArgument;
            $xml = simplexml_load_file($_SERVER['DOCUMENT_ROOT'].'/backend/resources/country/provinciasypoblaciones.xml');
            $result = $xml->xpath("/lista/provincia[@id='$filter']/localidades");

            for ($i=0; $i<count($result[0]); $i++) {
                $tmp = array(
                    'poblacion' => (string) $result[0]->localidad[$i]
                );
                array_push($json, $tmp);
            }
            return $json;
        }

        public function daoGETuserInfo($db, $userID) {
            return $db->createQuery("SELECT nickname, email, birthdate, province, town
                            FROM users
                            WHERE id = $userID;");;
        }

        public function daoGETlikes($db, $userID) {
            return $db->createQuery("SELECT *
                                    FROM Resources r
                                    WHERE r.IDResource IN (SELECT resourceID
                                                            FROM likeRes
                                                            WHERE userID = '$userID');");
        }

        public function daoGETpurchases($db, $userID) {
            $sql = "SELECT purchaseID, sum(price * quantity) totalPrice
                    FROM historyShop
                    WHERE userID = '$userID'
                    GROUP BY purchaseID;";
            return $db->createQuery($sql);
        }


        public function daoGETavatar($db, $userID) {
            return $db->createQuery("SELECT avatar
                                    FROM users
                                    WHERE id = $userID;");
        }


        public function daoPOSTupdateAvatar($db, $arrAguments) {
            $userID = $arrAguments['user'];
            $img = $arrAguments['img'];

            $sql = "UPDATE users
                    SET avatar = '$img'
                    WHERE id = '$userID';";

            $db->createQuery($sql);
        }

        public function daoPOSTupdateProfile($db, $info) {
            $userID = $info['userID'];

            $info['birthdate'] == null ? $birthdate = 'NULL' : $birthdate = "'" . $info['birthdate'] . "'";
            $info['province']  == null ? $province  = 'NULL' : $province = "'" . $info['province'] . "'";
            $info['town']      == null ? $town      = 'NULL' : $town = "'" . $info['town'] . "'";

            $sql = "UPDATE users SET
                    birthdate = $birthdate,
                    provice = $province,
                    town = $town
                    WHERE id = '$userID';";
                    
            return $db->createQuery($sql);
            
        }

    }
    