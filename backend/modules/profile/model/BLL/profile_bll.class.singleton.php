<?php
    class profile_bll {
        private $dao;
        private $db;

        static $_instance;

        private function __construct() {
            $this->dao = profile_dao::getInstance();
            $this->db = Db::getInstance();
        }

        public static function getInstance() {
            if (!(self::$_instance instanceof self)){
                self::$_instance = new self();
            }
            return self::$_instance;
        }
 
        private function getActiveUserID() {
            if (!isset($_SESSION['activeUser'])) {
                throw new Exception('User not logged');
            }
            $user = unserialize($_SESSION['activeUser']);
            return $user->getID();
        }

        private function executeDAO($functionName, $arrAguments = '') {
            if (method_exists($this->dao, $functionName)) {
                return $this->dao->$functionName($this->db, $arrAguments);
            } else {
                throw new Exception('Not exists DAO function -> '. $functionName);
            }
        }

        /////////GET FUNCTIONS//////////////

        public function resources($arrAguments) {
            return $this->executeDAO('daoGETResources');
        }

        public function provinces($arrAguments) {
            return $this->executeDAO('daoGETprovinces');
        }

        public function town($arrAguments) {
            return $this->executeDAO('daoGETtown', $arrAguments);
        }

        public function userInfo($data) {

            if (!isset($data['userToken'])) {
                throw new Exception('No token');
            }

            $token = $data['userToken'];
            $user = User::getUserByToken($token);


            if ($user == null) {
                throw new Exception('No user');
            }

            unset($user['passwd']);
            unset($user['emailToken']);
            unset($user['appToken']);

            echo json_encode($user);
            // return $this->executeDAO('daoGETuserInfo', $user['id']);
        }

        public function likes($data) {
            if (!isset($data['userToken'])) {
                throw new Exception('No token');
            }

            $token = $data['userToken'];
            $user = User::getUserByToken($token);
            return $this->executeDAO('daoGETlikes', $user['id']);
        }

        public function purchases($data) {
            if (!isset($data['userToken'])) {
                throw new Exception('No token');
            }

            $token = $data['userToken'];
            $user = User::getUserByToken($token);
            return $this->executeDAO('daoGETpurchases', $user['id']);
        }

        public function avatar($arrAguments) {
            return $this->executeDAO('daoGETavatar', $this->getActiveUserID());
        }


        /////////////POST FUNCTIONS//////////////////////

        public function updateAvatar($data) {
            $info = array(
                "img"  => $data['img'],
                "user" => $data['user']
            );

            return $this->executeDAO('daoPOSTupdateAvatar', $info);
        }

        public function updateProfile($data){
            $token = $data['token'];

            $userID = User::getUserByToken($token)['id'];

            $info = array(
                'userID'    => $userID,
                'birthdate' => $data['birthdate'],
                'province'  => $data['province'],
                'town'      => $data['town']
            );

            $this->executeDAO('daoPOSTupdateProfile', $info);

            $newToken = User::updateAppToken($token);

            Response::ok($newToken);
        }

        public function uploadAvatar() {
            $result_prodpic = upload_files($_SERVER['DOCUMENT_ROOT'].'/backend/media/userAvatar/');
            $_SESSION['newAvatar'] = '/backend' . $result_prodpic['data'];
        }

        public function setAvatar($data) {
            $token = $data['token'];
            $user = User::getUserByToken($token);
            
            $_SESSION['oldAvatar'] = $user['avatar'];
      
            $data = array (
                'img'  => $_SESSION['newAvatar'],
                'user' => $user['id']
            );

            unset($_SESSION['newAvatar']);

            $this->updateAvatar($data);
        }

        public function deleteAvatar($data) {
            $token = $data['token'];
            $user = User::getUserByToken($token);
            
            $data = array (
                'img'  => $_SESSION['oldAvatar'],
                'user' => $user['id']
            );

            unset($_SESSION['oldAvatar']);

            $this->updateAvatar($data);
        }

    }
    