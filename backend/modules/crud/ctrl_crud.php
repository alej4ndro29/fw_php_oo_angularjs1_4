<?php
class crud {
    
    const PATH_MODEL = __DIR__ . '/model/model/';
    static $_instance;

    function __construct() {}

    public static function getInstance() {
        if (!(self::$_instance instanceof self))
            self::$_instance = new self();
        return self::$_instance;
    }

    public function __get($property) {
        if (property_exists($this, $property)) {
            return $this->$property;
        }
    }

    public function getResources() {
        try {
            loadModel(self::PATH_MODEL, 'crud_model', 'post', 'getResources', $_POST);
        } catch (Exception $e) {
            Response::nok($e->getMessage());
        }

    }

    public function getResource() {
        try {
            loadModel(self::PATH_MODEL, 'crud_model', 'post', 'getResource', $_POST);
        } catch (Exception $e) {
            Response::nok($e->getMessage());
        }
    }

    public function create() {
        try {
            loadModel(self::PATH_MODEL, 'crud_model', 'post', 'create', $_POST);
        } catch (Exception $e) {
            Response::nok($e->getMessage());
        }
    }

    public function update() {
        try {
            loadModel(self::PATH_MODEL, 'crud_model', 'post', 'update', $_POST);
        } catch (Exception $e) {
            Response::nok($e->getMessage());
        }
    }

    public function delete() {
        try {
            loadModel(self::PATH_MODEL, 'crud_model', 'post', 'delete', $_POST);
        } catch (Exception $e) {
            Response::nok($e->getMessage());
        }
    }

}