<?php
    class crud_bll {
        private $dao;
        private $db;

        static $_instance;

        private function __construct() {
            $this->dao = crud_dao::getInstance();
            $this->db = Db::getInstance();
        }

        public static function getInstance() {
            if (!(self::$_instance instanceof self)){
                self::$_instance = new self();
            }
            return self::$_instance;
        }

        private function executeDAO($functionName, $arrAguments = '') {
            if (method_exists($this->dao, $functionName)) {
                return $this->dao->$functionName($this->db, $arrAguments);
            } else {
                throw new Exception('Not exists DAO function -> '. $functionName);
            }
        }

        private function isAdminUser() {
            $token = $_POST['token'];
            $user = User::getUserByToken($token);
            if($user['typeUser'] != 'admin') {
                throw new Exception("no_admin");
            }
        }

        public function getResources($data) {
            $this->isAdminUser();
            $resources = $this->executeDAO('getResources');
            Response::ok($resources);
        }

        public function getResource($data) {
            $this->isAdminUser();
            $resource = $this->executeDAO('getResource', $data);
            Response::ok($resource);
        }

        public function create($data) {
            $this->isAdminUser();
            $response = $this->executeDAO('create', $data);
            Response::ok($response);
        }

        public function update($data) {
            $this->isAdminUser();
            $response = $this->executeDAO('update', $data);
            Response::ok($response);
        }

        public function delete($data) {
            $this->isAdminUser();
            $response = $this->executeDAO('delete', $data);
            Response::ok($response);
        }

    }
    