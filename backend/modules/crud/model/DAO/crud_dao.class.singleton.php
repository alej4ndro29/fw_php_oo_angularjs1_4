<?php
    class crud_dao {
        static $_instance;

        private function __construct() {
        }

        public static function getInstance() {
            if(!(self::$_instance instanceof self)) {
                self::$_instance = new self();
            }
            return self::$_instance;
        }


        public function getResources($db) {
            $sql = "SELECT IDResource, nameResource, ISBNResource, releaseResource
                    FROM Resources";
            $result = $db->createQuery($sql);
            return $db->listQuery($result);
        }

        public function getResource($db, $data) {
            $id = $data['id'];
            $sql = "SELECT *
                    FROM Resources
                    WHERE IDResource = '$id'";
            $result = $db->createQuery($sql);
            return $db->listOne($result);
        }

        public function create($db, $data) {
            $type = $data['type'];
            $name = $data['name'];
            $editorial = $data ['editorial'];
            $isbn = $data['isbn'];
            $release = $data['release'];
            $genre = $data['genre'];
            $price = $data['price'];

            $sql = "INSERT INTO `Resources`
                (
                    `typeResource`,
                    `nameResource`,
                    `editorialResource`,
                    `ISBNResource`,
                    `releaseResource`,
                    `generoResource`,
                    `dateAddedResource`,
                    `price`
                )
                VALUES
                (
                    '$type',
                    '$name',
                    '$editorial',
                    '$isbn',
                    '$release',
                    '$genre',
                    CURRENT_DATE(),
                    '$price'
                );";

            $db->createQuery($sql);
        }

        public function update($db, $data) {
            $type = $data['type'];
            $name = $data['name'];
            $editorial = $data ['editorial'];
            $isbn = $data['isbn'];
            $release = $data['release'];
            $genre = $data['genre'];
            $price = $data['price'];

            $id = $data['id'];
            $sql = "UPDATE Resources
                    SET
                        typeResource = '$type',
                        nameResource = '$name',
                        editorialResource = '$editorial',
                        ISBNResource = '$isbn',
                        releaseResource = '$release',
                        generoResource = '$genre',
                        price = '$price'
                    WHERE `IDResource` = '$id';";

            $db->createQuery($sql);
        }


        public function delete($db, $data) {
            $id = $data['id'];
            $sql = "DELETE FROM Resources
                    WHERE IDResource = '$id';";
            
            $db->createQuery($sql);
        }
    }
    