<?php
    /* SERVER PATH (/opt/lampp/htdocs) */
    define('APP_PATH', __DIR__ . '/../');

    /* SITE PATH */
    define('SITE_PATH', 'http://' . $_SERVER['HTTP_HOST']);

    /* SITE NAME */
    define('SITE_NAME', 'http://' . $_SERVER['SERVER_NAME']);

    /* MODEL DB PATH */
    define('MODEL_DB_PATH', $_SERVER['DOCUMENT_ROOT'] . '/model/db');

    /* MODULES PATH */
    define('MODULES_PATH' , APP_PATH . '/modules');

    /* CONFIG PATH */
    define('CONF_PATH', APP_PATH . '/config');

    /* VIEW PATH */
    define('VIEW_PATH', APP_PATH . '/view');

    /* VIEW INCLUDE PATH */
    define('VIEW_INC_PATH', VIEW_PATH . '/inc');

    /* VIEW IMG PATH */
    define('VIEW_IMG_PATH', VIEW_PATH . '/img');

    /* VIEW CSS PATH */
    define('VIEW_CSS_PATH', VIEW_PATH . '/css');

    /* VIEW JS PATH */
    define('VIEW_JS_PATH', VIEW_PATH . '/js');