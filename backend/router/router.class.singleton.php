<?php
    class Router {
        private $components;
        private $modules;
        private $functions;
        static $_instance;
        

        function __construct() {
            $this->components = simplexml_load_file(__DIR__.'/components.xml');
            $this->modules    = simplexml_load_file(__DIR__.'/modules.xml');
            $this->functions  = simplexml_load_file(__DIR__.'/functions.xml');
        }

        public static function getInstance() {
            if (!(self::$_instance instanceof self))
                self::$_instance = new self();
            return self::$_instance;
        }
        
        public function __get($property) {
            if (property_exists($this, $property)) {
                return $this->$property;
            }
        }

        private function initialFunctions() {
            session_start();
            @include APP_PATH . '/config/constants.php';
            
            // CREAR CARRITO SI NO EXISTE
            if (!isset($_SESSION['cart'])) {
                $_SESSION['cart'] = array();
            }

        }

        private function componentExists() {
            foreach ($this->components as $name) {
                if ($name->name == $_GET['op']) {
                    return true;
                }
            }

            return false;
        }

        private function getComponent() {
            $component = $_GET['op']::getInstance();
            if (!isset($_GET['aux'])) {
                header('HTTP/1.0 400 Bad error');
            } else {
                $funExists = $this->componentFunctionExists();
                if ($funExists) {
                    $component->$_GET['aux']();
                } else {
                    error_log('Function not added');
                    header('HTTP/1.0 400 Bad error');
                }
            }
        }

        private function componentFunctionExists() {
            $componentFunctions = $this->functions->components->$_GET['op']->function;
            foreach ($componentFunctions as $key) {
                if ($key == $_GET['aux']) {
                    return true;
                }
            }
            
            return false;
        }

        private function moduleExists() {
            foreach ($this->modules as $name) {
                if ($name->name == $_GET['module']) {
                    return true;
                }
            }
            return false;
        }

        private function moduleFunctionExists() {
            $moduleFunctions = $this->functions->modules->$_GET['module']->function;
            foreach ($moduleFunctions as $funcName) {
                if ($funcName == $_GET['op']) {
                    return true;
                }
            }

            return false;
        }

        private function showError($code) {
            switch ($code) {
                case 404:
                    echo 'ERROR 404: not found';
                    break;
            }
        }

        private function getModule() {
            $module = $_GET['module']::getInstance();
            if (!isset($_GET['op'])) {
                // LOAD VIEW IF NOT FUNCTION
                /* OLD METHOD */
                // $module->loadView();
            } else {
                $funExists = $this->moduleFunctionExists();
                if ($funExists) {
                    $module->$_GET['op']();
                } else {
                    error_log('Function not added');
                    $this->showError('404');
                }
            }
        }


        public function run() {
            $this->initialFunctions();

            $_POST = json_decode(file_get_contents('php://input'), true);

            if (!isset($_GET['module']) || $_GET['module'] == "") {
                // LOAD HOME IF MODULE NOT EXISTS
                //die('<script>window.location.href = "/homePage";</script>');
                die;
            }

            if($_GET['module'] == 'component') { // Diference component and module name
                $componentExists = $this->componentExists();
                if($componentExists) {
                    $this->getComponent();
                } else {
                    error_log('Component not added - XML');
                    header('HTTP/1.0 400 Bad error');
                }

            } else {
                $moduleExists = $this->moduleExists();
                if ($moduleExists) {
                    $this->getModule();
                } else {
                    error_log(print_r($_GET, true));
                    error_log('Module not added - XML ');
                    $this->showError('404');
                }
            }

        }

    }