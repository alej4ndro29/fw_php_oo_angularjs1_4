var bookapp = angular.module('bookapp', ['ngRoute', 'angularUtils.directives.dirPagination', 'ui.bootstrap']);

bookapp.config(function ($routeProvider) {
    $routeProvider
        .when('/', {
            templateUrl: 'frontend/modules/homePage/view/homePage.html',
            controller: 'homePageCtrl',
            resolve: {
                resource: function (services) {
                    return services.get('homePage', 'getItems');
                }
            }
        })

        .when('/contact', {
            templateUrl: 'frontend/modules/contact/view/contact.html',
            controller: 'contactCtrl',
        })

        .when('/details/:id', {
            templateUrl: 'frontend/modules/details/view/details.html',
            controller: 'detailsCtrl',
            resolve: {
                item: function (services, $route) {
                    return services.post('shop', 'getItem', {item: $route.current.params.id});
                }
            }
        })

        .when('/login', {
            templateUrl: 'frontend/modules/login/view/login.html',
            controller: 'loginCtrl'
        })

        .when('/login/activate/:token', {
            templateUrl: 'frontend/modules/login/view/activate.html',
            controller: 'activateCtrl',
            resolve: {
                response: function (services, $route) {
                    return services.post('login', 'activate', {token: $route.current.params.token});
                }
            }
        })

        .when('/login/recover/:token', {
            templateUrl: 'frontend/modules/login/view/recover.html',
            controller: 'recoverCtrl'
        })

        .when('/profile', {
            templateUrl: 'frontend/modules/profile/view/profile.html',
            controller: 'profileCtrl',
            resolve: {
                profile: function ($window, userToken, services) {
                    if(userToken.getToken() == null) {
                        $window.location.href = "#/login";
                    } else {
                        return services.post('profile', 'loadUserInfo', { userToken: userToken.getToken() });
                    }
                }
            }
        })

        .when('/search/:q', {
            templateUrl: 'frontend/modules/search/view/search.html',
            controller: 'searchCtrl',
            resolve: {
                results: function (services, $route) {
                    return services.postComponent('search', 'searchParam', {inp: $route.current.params.q});
                }
            }
        })

        .when('/order', {
            templateUrl: 'frontend/modules/order/view/order.html',
            controller: 'orderCtrl',
            resolve: {
                results: function (services) {
                    return services.get('order', 'getData');
                }
            }
        })

        .when('/cart', {
            templateUrl: 'frontend/modules/cart/view/cart.html',
            controller: 'cartCtrl',
            resolve: {
                cart: function (serviceCart) {
                       return serviceCart.getList();  
                }
            }
        })

        .when('/crud', {
            templateUrl: 'frontend/modules/crud/view/crud.html',
            controller: 'crudCtrl',
            resolve: {
                resources: function ($window, services, userToken) {
                    if (userToken.getToken() == null) {
                            $window.location = '#/login';
                            return null;
                        } else {
                            return services.post('crud', 'getResources', {token: userToken.getToken()});
                    }
                }
            }      
        })

        .when('/crud/create', {
            templateUrl: 'frontend/modules/crud/view/create.html',
            controller: 'createCtrl'
        })

        .when('/crud/update/:id', {
            templateUrl: 'frontend/modules/crud/view/create.html',
            controller: 'updateCtrl',
            resolve: {
                resource: function ($route, $window, services, userToken) {
                    if (userToken.getToken() == null) {
                        $window.location = '#/login';
                        return null;
                    } else {
                        var data = {token: userToken.getToken(), id: $route.current.params.id}
                        return services.post('crud', 'getResource', data);
                    }
                }
            }
        })

        .when('/test', {
            templateUrl: 'frontend/modules/test/test.html',
            controller: 'testCtrl'
        })

        .otherwise({
            templateUrl: 'frontend/modules/test/test.html',
            controller: 'testCtrl'
        })
});
