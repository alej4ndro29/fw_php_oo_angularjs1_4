bookapp.factory('userToken', function (services) {
    var proc = {};

    proc.setToken = function (token) {
        localStorage.setItem('userToken', token);
    }

    proc.getToken = function () {
        return localStorage.getItem('userToken');
    }

    proc.removeToken = function () {
        localStorage.removeItem('userToken');
    }

    proc.getUser = function () {
        var data = {userToken: proc.getToken()};
        return services.post('login', 'getUserByToken', data);

    }

    return proc;
})