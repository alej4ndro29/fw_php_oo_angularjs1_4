bookapp.factory('serviceCart', function (services, userToken) {
    var serv = {};

    serv.addToCart = function (data) {
        return services.post('cart', 'addToCart', data);
    }

    serv.getList = function () {
        return services.get('cart','getList').then(function (response) {

            var articleList = new Array();

            for (var i = 0; i < response.length; i++) {
                var prodID = response[i]['productID'];

                serv.getElementList(prodID).then(function (response) {
                    response.inCart = true;
                    articleList.push(response);
                });
            }

            return articleList;
        });
    }

    serv.getElementList = function (prodID) {
        return services.getAux('cart', 'getElementList', prodID);
    }

    serv.addOne = function (prodID) {
        var token = userToken.getToken();
        return services.post('cart', 'addOne', {item: prodID, token:token});
    }

    serv.minusOne = function (prodID) {
        var token = userToken.getToken();
        return services.post('cart', 'minusOne', {item: prodID, token:token});
    }

    serv.removeItem = function (prodID) {
        var token = userToken.getToken();
        return services.post('cart', 'removeItem', {item: prodID, token:token});
    }

    serv.completeOrder = function () {
        var token = userToken.getToken();
        return services.post('cart', 'completeOrder', {userToken:token});
    }

    return serv;
})