bookapp.factory("services", ['$http', '$q', function ($http, $q) {
    var serviceBase = 'backend/index.php?module='
    var obj = {};

    obj.test = function () {
        console.log('services test')
    }

    obj.get = function (module, funct) {
        var defered = $q.defer();
        var promise = defered.promise;
        $http({
            method: 'GET',
            url: serviceBase + module + '&op=' + funct
        })
            .success(function (data, status, headers, config) {
                defered.resolve(data);
            })
            .error(function (data, status, headers, config) {
                defered.reject(data);
            });
        return promise;
    }

    obj.getAux = function (module, funct, aux) {
        var defered = $q.defer();
        var promise = defered.promise;
        $http({
            method: 'GET',
            url: serviceBase + module + '&op=' + funct + '&aux=' + aux
        })
            .success(function (data, status, headers, config) {
                defered.resolve(data);
            })
            .error(function (data, status, headers, config) {
                defered.reject(data);
            });
        return promise;
    }

    obj.getUrl = function (url) {
        var defered = $q.defer();
        var promise = defered.promise;
        $http({
            method: 'GET',
            url: url
        })
            .success(function (data, status, headers, config) {
                defered.resolve(data);
            })
            .error(function (data, status, headers, config) {
                defered.reject(data);
            });
        return promise;
    }

    obj.post = function (module, funct, postData) {
        var defered = $q.defer();
        var promise = defered.promise;
        $http({
            method: 'POST',
            url: serviceBase + module + '&op=' + funct,
            data: postData
        })
            .success(function (data, status, headers, config) {
                defered.resolve(data);
            })
            .error(function (data, status, headers, config) {
                defered.reject(data);
            });
        return promise;
    }

    obj.postComponent = function (component, funct, postData) {
        // /backend/index.php?module=component&op=search&aux=search
        var defered = $q.defer();
        var promise = defered.promise;
        $http({
            method: 'POST',
            url: serviceBase + 'component&op=' + component + '&aux=' + funct,
            data: postData
        })
            .success(function (data, status, headers, config) {
                defered.resolve(data);
            })
            .error(function (data, status, headers, config) {
                defered.reject(data);
            });
            
        return promise;        
    }

    return obj;
}]);