bookapp.controller('profileCtrl', function ($scope, userToken, services, profile) {

    $scope.nickname = profile.nickname;
    $scope.email = profile.email;
    
    $scope.birthdate = profile.birthdate;
    $scope.formatDate = 'yyyy-MM-dd';

    $scope.currentProvince = profile.provice;
    $scope.provinceModify = false;
    $scope.currentTown = profile.town;

    $scope.townDisable = true;
    $scope.likeLoaded = false;
    $scope.likesInfo = null;
    $scope.purchasesInfo = null;
    $scope.purchasesLoaded = false;

    $scope.currentPageLikes = 1;


    $scope.test = function () {
        // var token = userToken.getToken();
        // console.log(token);
    }

    // Tabs
    $scope.profile = function () {
        $scope.active = 'profile';
    }

    // Likes
    $scope.likes = function () {
        $scope.active = 'likes';
        // if (!$scope.likeLoaded) {
            data = { userToken: userToken.getToken() };
            services.post('profile', 'loadLikes', { userToken: userToken.getToken() })
                .then(function (response) {
                    $scope.likesInfo = response;
                    $scope.likeLoaded = true;
                })
        // }
    }
 
    // Purchases
    $scope.purchases = function () {
        $scope.active = 'purchases';
        
        // if (!$scope.purchasesLoaded) {
            services.post('profile', 'loadPurchases', { userToken: userToken.getToken() })
                .then(function (response) {
                    $scope.purchasesInfo = response;
                    $scope.purchasesLoaded = true;
                });
        // }
    }

    // Default tab
    $scope.profile();

    $scope.checkActive = function (tab) {
        return tab == $scope.active;
    }

    $scope.datePickerOpen = function () {
        $scope.isOpened = true;
    }

    if(profile.province == null || profile.province == '') {
        services.get('profile', 'loadProvinces').then(function (provinces) {
            $scope.provinces = provinces;
        });
    }
    

    $scope.$watch('province', function (provinceSelected) {
        $scope.towns = null;
        $scope.townDisable = true;
        if ($scope.province == '') {
            $scope.province = null;
        }
        if (provinceSelected != null && provinceSelected != '') {
            provinceSelected = JSON.parse(provinceSelected);
            $scope.loadTown(provinceSelected);
        }
    });

    $scope.provinceModifyProcess = function () {
        $scope.provinceModify = true;
        $scope.townModify = true;
    }

    $scope.loadTown = function (idProvince) {
        services.getAux('profile', 'loadTown', idProvince.id).then(function (towns) {
            $scope.towns = towns;
            $scope.townDisable = false;
        });
    }

    $scope.townModifyProcess = function () {
        $scope.townModify = true;
    }

    // Dropzone
    $scope.dropzoneConfig = {
        'options': {
            'url': 'backend/?module=profile&op=uploadAvatar',
            addRemoveLinks: true,
            maxFileSize: 3000,
            dictResponseError: 'Server error',
            acceptedFiles: 'image/*,.jpeg,.jpg,.png,.gif,.JPEG,.JPG,.PNG,.GIF'
        },
        'eventHandlers': {
            'sending': function (file, formData, xhr) { },
            'success': function (file, response) {
                console.log(response);
                services.post('profile', 'setAvatar', {token: userToken.getToken()}).then(function (response) {
                    console.log(response);  
                });
            },
            'removedfile': function (file, serverFilename) {
                services.post('profile', 'deleteAvatar', { token: userToken.getToken() }).then(function (response) {
                    console.log(response);
                });
            }
        }
    }


    $scope.updateProfile = function () {
        $scope.updatingProfile = true;
        var province;
        var town = $scope.town;

        if ($scope.provinceModify) {
            if (town == null) {
                town = null;
            }
            try {
                province = JSON.parse($scope.province).nombre;
            } catch (e) {
                province = null;
                town = null;
            }
        } else {
            province = $scope.currentProvince;
            town = $scope.currentTown;
        }

        var data = {
            token: userToken.getToken(),
            birthdate: $scope.birthdate,
            province: province,
            town: town
        };

        services.post('profile', 'updateProfile', data).then(function (response) {
            userToken.setToken(response.message);
            $scope.updatingProfile = false;
            toastr.success('Profile updated');
        });

    }

});