bookapp.controller('updateCtrl', function ($scope, $window, services, userToken, resource) {

    resource = resource.message;
    
    var user = JSON.parse(sessionStorage.getItem('user'));
    if (user == null || user.typeUser != 'admin') {
        $window.location = '#/login';
    }

    $scope.genre = {
        aventuras : false,
        accion : false,
        terror : false,
        otro : false
    }

    $scope.formatDate = 'yyyy-MM-dd';
    $scope.datePickerOpen = function () {
        $scope.isOpened = true;
    }

    $scope.type = resource.typeResource;
    $scope.name = resource.nameResource;
    $scope.editorial = resource.editorialResource;
    $scope.isbn = resource.ISBNResource;
    $scope.release = resource.releaseResource;
    $scope.price =  resource.price;

    if (resource.generoResource.includes('aventuras')) $scope.genre.aventuras = true;
    if (resource.generoResource.includes('accion')) $scope.genre.accion = true;
    if (resource.generoResource.includes('terror')) $scope.genre.terror = true;
    if (resource.generoResource.includes('otro')) $scope.genre.otro = true;

    $scope.save = function () {
        var genre = "";

        if($scope.genre.aventuras) {
            if(genre != "") genre += ':';
            genre += 'aventuras';
        }

        if($scope.genre.accion) {
            if(genre != "") genre += ':';
            genre += 'accion';
        }

        if($scope.genre.terror) {
            if(genre != "") genre += ':';
            genre += 'terror';
        }

        if($scope.genre.otro) {
            if(genre != "") genre += ':';
            genre += 'otro';
        }

        var data = {
            token: userToken.getToken(),
            name: $scope.name,
            editorial: $scope.editorial,
            release: $scope.release,
            genre: genre,
            isbn: $scope.isbn,
            type: $scope.type,
            price: $scope.price,
            id: resource.IDResource
        }
        services.post('crud', 'update', data).then(function (response) {
            if(response.correct) {
                toastr.success('Saved');
                $window.history.back();
            } else {
                toastr.error(response.message);
            }
        });
    }

})