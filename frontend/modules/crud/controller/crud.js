bookapp.controller('crudCtrl', function ($scope, $window, $route, services, resources, userToken) {

    if(resources.correct == false && resources.message == 'no_admin') {
        $window.location = '#/';
        // alert('Unauthorized');
    } else {
        $scope.resources = resources.message;
    }
    
    $scope.delete = function (resource) {
        
        // if(confirm("Do you really want to do this?")) {
            var data = {token: userToken.getToken(), id: resource.IDResource};
            console.log(data);
            services.post('crud', 'delete', data).then(function (response) {
                toastr.success('Deleted correctly');
                console.log(response);
                if(response.correct) $route.reload();
            })
        // }
    }

})