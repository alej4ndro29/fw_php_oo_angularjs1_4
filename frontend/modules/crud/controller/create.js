bookapp.controller('createCtrl', function ($scope, $window, userToken, services) {
    
    $scope.isCreating = true;
    $scope.formatDate = 'yyyy-MM-dd';

    $scope.genre = {
        aventuras : false,
        accion : false,
        terror : false,
        otro : false
    }

    var user = JSON.parse(sessionStorage.getItem('user'));

    if (user == null || user.typeUser != 'admin') {
        $window.location = '#/login';
    }

    $scope.datePickerOpen = function () {
        $scope.isOpened = true;
    }

    $scope.searchApi = function () {
        if ($scope.isbn == null) {
            toastr.warning('Invalid ISBN');
        } else {
            var url = 'https://www.googleapis.com/books/v1/volumes?q=' + $scope.isbn;
            
            console.log(url);

            services.getUrl(url).then(function (response) {
                $scope.name = response['items']['0']['volumeInfo']['title'];
                $scope.editorial = response['items']['0']['volumeInfo']['publisher'];
                $scope.release = response['items']['0']['volumeInfo']['publishedDate'];
                if ($scope.release != null && $scope.release.length == '4') { 
                    // SI LA API SOLO OFRECE EL AÑO LE PONEMOS UNO DE ENERO POR DEFECTO
                    $scope.release = "01-01-" + $scope.release;
                }
                typeAPI = response['items']['0']['volumeInfo']['printType'];
                $scope.type = "libro";
                $scope.genre = {otro: true}
            });

        }
    }

    $scope.save = function () {
        var genre = "";

        if($scope.genre.aventuras) {
            if(genre != "") genre += ':';
            genre += 'aventuras';
        }

        if($scope.genre.accion) {
            if(genre != "") genre += ':';
            genre += 'accion';
        }

        if($scope.genre.terror) {
            if(genre != "") genre += ':';
            genre += 'terror';
        }

        if($scope.genre.otro) {
            if(genre != "") genre += ':';
            genre += 'otro';
        }

        var data = {
            token: userToken.getToken(),
            name: $scope.name,
            editorial: $scope.editorial,
            release: $scope.release,
            genre: genre,
            isbn: $scope.isbn,
            type: $scope.type,
            price: $scope.price
        }
        
        services.post('crud', 'create', data).then(function (response) {
            toastr.success('Saved');
            $scope.clear();
        });
    }

    $scope.clear = function () {
        $scope.type = "";
        $scope.genre = "";
        $scope.name = "";
        $scope.editorial = "";
        $scope.isbn = "";
        $scope.release = "";
        $scope.price = "";
    }

})