bookapp.controller('searchCtrl', function ($scope, results) {

    $scope.items = results;
    $scope.currentPage = 1;
    $scope.pageSize = 3;
  

    $scope.isResultsEmpty = function () {
        return results.length == 0;
    }

});

