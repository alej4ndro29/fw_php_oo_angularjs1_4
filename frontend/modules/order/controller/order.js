bookapp.controller('orderCtrl', function ($scope, $window, services, results, userToken, serviceCart) {

    // console.log(results);

    $scope.resources = results;

    $scope.currentPage = 1;
    $scope.pageSize = 5;

    $scope.likeDisabled = false;
    $scope.cartDisabled = false;

    $scope.like = function (resource) {
        var token = userToken.getToken();
        if(token == null) {
            $window.location.href = '#/login';
        } else {
            $scope.likeDisabled = true;
            var data = {idResource: resource.IDResource, userToken: token}
            
            services.post('order', 'newLike', data).then(function (response) {
                userToken.setToken(response.newToken);
                
                switch (response.message) {
                    case 'addedLike':
                        resource.likes++;
                        break;
                    case 'removedLike':
                        resource.likes--;
                        break;
                    default:
                        break;
                }
                $scope.likeDisabled = false;
            });

        }
    }

    $scope.cart = function (resource) {
        $scope.cartDisabled = true;
        var item = resource.IDResource;
        var token = userToken.getToken();

        var data;
        if (token == null) {
            data = { item: item };
        } else {
            data = { item: item, token: token };
        }

        serviceCart.addToCart(data).then(function () {
            $scope.cartDisabled = false;
            toastr.success("Added to cart");
        });
    }
})