bookapp.controller('homePageCtrl', function ($scope, $window, services, resource) {

    var searchParams = {
        type: null,
        genre: null,
        text: null
    };

    $scope.autocomplete = null;
    $scope.resource = resource;

    $scope.$watch('homeSearch', function (text) {
        if (text == '' || text == null) {
            searchParams.text = null;
            $scope.autocomplete = null;
            checkSearchNull();
        } else if(text.length >= 13) {
            $scope.autocomplete = null;
        } else {
            searchParams.text = text;
            $scope.autocomplete = resource;
            search();
        }
    });

    $scope.$watch('searchType', function (type) {
        if (type == '' || type == null) {
            searchParams.type = null;
            checkSearchNull();
        } else {
            searchParams.type = type;
            search();
        }
    });

    $scope.$watch('searchGenre', function (genre) {
        if (genre == '' || genre == null) {
            searchParams.genre = null;
            checkSearchNull();
        } else {
            searchParams.genre = genre;
            search();
        }
    });

    
    $scope.onlyMatch = function (input, output) {
        var Str = (input + "").toLowerCase();
        return Str.indexOf(output.toLowerCase()) === 0;
    };

    $scope.autocompleteClick = function (code) {
        $scope.homeSearch = code;
        searchParams.text = code;
        search();
    };

    $scope.details = function (data) {
        $window.location = '#/details/' + data.IDResource;
    };
    
    function checkSearchNull() {
        if (searchParams.type == null && searchParams.genre == null && searchParams.inpText == null) {
            $scope.resource = resource;
        }
    }

    function search() {
        $scope.resource = [];
        var inpType  = searchParams.type;
        var inpGenre = searchParams.genre;
        var inpText  = searchParams.text;

        for (var i = 0; i < resource.length; i++) {
            var type  = resource[i].typeResource;
            var genre = resource[i].generoResource
            var name  = resource[i].nameResource;
            var isbn  = resource[i].ISBNResource;

            var checkType  = (type == inpType);
            var checkGenre = (genre.includes(inpGenre));
            
            var checkText  = (inpText != null && name.toLowerCase().includes(inpText.toLowerCase()));
            var checkIsbn  = (inpText != null && isbn.includes(inpText.toLowerCase()));

            if(checkType || checkGenre || checkText || checkIsbn) {
                $scope.resource.push(resource[i])
            }
    
        }

    }
});