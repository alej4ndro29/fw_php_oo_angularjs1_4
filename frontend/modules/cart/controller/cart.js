bookapp.controller('cartCtrl', function ($scope, serviceCart, cart, userToken, $window, $timeout) {
    $scope.cart = cart;
    $scope.removeQuantity = 0;
    
    $scope.test = function () {
        console.log($scope.cart[1]);
        console.log($scope.cart);
        console.log($scope.cart.length);
    }

    $scope.isEmptyCart = function () {
        return ($scope.cart == null || $scope.cart.length == 0 || $scope.cart.length == $scope.removeQuantity);
    }

    $scope.minusOne = function (item) {
        serviceCart.minusOne(item.id).then(function (response) {
            if(response) {
                item.quantity--;                
                if(item.quantity == 0) {
                    item.inCart = false;
                    toastr.info("Removed item");
                    $scope.removeQuantity++;
                } else {
                    toastr.info("Decreased item");
                }
            }
        });
    }

    $scope.addOne = function (item) {
        serviceCart.addOne(item.id).then(function (response) {
            if(response) {
                item.quantity++;
                toastr.info("Increased item");
            }
        });
    }

    $scope.removeItem = function (item) {
        serviceCart.removeItem(item.id).then(function (response) {
            if(response) {
                item.quantity = 0;
                item.inCart = false;
                $scope.removeQuantity++;
                toastr.info("Removed item");
            }
        });
    }

    $scope.completeOrder = function () {
        if(userToken.getToken() == null) {
            $window.location.href = '#/login';
        } else {
            serviceCart.completeOrder().then(function () {
                toastr.info("Completed order");
                $scope.cart = null;
            })
        }
    }

    $scope.calculateTotal = function () {
        var total = 0;
        $scope.cart.forEach(element => {
            if (element.inCart) {
                var price = parseFloat(element.price)
                price = price * element.quantity
                total += price;
            }
        });
        
        return total.toString().match(/^-?\d+(?:\.\d{0,2})?/)[0];
    }

});