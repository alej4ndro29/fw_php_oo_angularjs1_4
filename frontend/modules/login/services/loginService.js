bookapp.factory('loginService', function ($window, services, userToken, $location) {
   var service = {};
   
    service.socialLogin = function (userI) {
        services.post("login", "socialLogin", userI).then(function (data) {
            var token = data.userToken;
            userToken.setToken(token);

            $window.location.href = "#/";
            $window.location.reload();
        });
    }

    service.login = function (data) {
        services.post('login', 'login', data).then(function (response) {
            if(response.correct) {
                userToken.setToken(response.token);

                $window.location.href = "#/";
                $window.location.reload();
            } else {
                toastr.error(response.message); 
            }
            
        })
    }

    service.registerUser = function (data) {
        services.post('login', 'register', data).then(function (response) {
            if (response.correct) {
                toastr.success('Check you mail for active your account');
            } else {
                if (response.message == "userAlredyRegistered") {
                    toastr.error('User alredy registered');
                }
            }
        })
    }

    service.recover = function (data) {
        services.post('login', 'recPasswd', data).then(function (response) {
            toastr.success('Check you mail for recover password');
        })
    }

   return service;
});