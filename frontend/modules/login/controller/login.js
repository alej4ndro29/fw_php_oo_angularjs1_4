var webAuth;
var userInfo;

function inicialiceWebAuth() {
    webAuth = new auth0.WebAuth({
        domain: 'alej4-dev.eu.auth0.com',
        clientID: 'HPa7flJZeZuk99N2ycB9jDC6AzCpTOlb',
        responseType: 'token id_token',
        redirectUri: 'http://localhost1/login/'
    });
}

function socialLoginPopup(err, authResult) {
    if (authResult && authResult.accessToken && authResult.idToken) {
        userInfo = authResult;
    } else {
        userInfo = null;
    }
}

function socialLogout() {
    localStorage.removeItem('socialLogin');
    webAuth.logout({
        returnTo: window.location.href,
        client_id: 'HPa7flJZeZuk99N2ycB9jDC6AzCpTOlb'
    });
}

bookapp.controller('loginCtrl', function ($scope, loginService, userToken) {

    inicialiceWebAuth();

    $scope.formData = {};
    $scope.recShow = false;

    if (userToken.getToken() != null || localStorage.getItem('socialLogin')) {
        localStorage.removeItem('socialLogin');
        userToken.removeToken();
        socialLogout();
    }


    $scope.socialLogin = function () {
        webAuth.popup.authorize({}, function () {
            localStorage.setItem('socialLogin', true);
            if(userInfo != null) {
                loginService.socialLogin(userInfo);
            } 
        });
    };

    $scope.socialLogout = function () {
        socialLogout();
    };


    $scope.login = function () {
        var ok = true;

        if ($scope.formData.lgNick == null || $scope.formData.lgNick == '') {
            toastr.warning("Introduce valid user");
            ok = false;
        }

        if ($scope.formData.lgPassword == null || $scope.formData.lgPassword == '') {
            toastr.warning("Introduce valid password");
            ok = false;
        }

        if (ok) {
            var data = {
                'lg-nick': $scope.formData.lgNick,
                'lg-password': $scope.formData.lgPassword
            };
            loginService.login(data);
        }

    }

    // Detect login enter
    $scope.loginKeydown = function (keyCode) {
        if (keyCode == 13) {
            $scope.login();
        }
    }

    // Register
    $scope.register = function () {
        var ok = true;
        var data = {
            'reg-nick': $scope.regNick,
            'reg-email': $scope.regEmail,
            'reg-password': $scope.regPassword1,
            'reg-password-rep': $scope.regPassword2
        }

        if ($scope.regPassword1 != $scope.regPassword2 || $scope.regPassword1 == null) {
            toastr.warning('Password not match');
            ok = false;
        }

        if ($scope.regNick == null) {
            toastr.warning('Invalid nick');
            ok = false;
        }

        if ($scope.regEmail == null) {
            toastr.warning('Invalid email');
            ok = false;
        }

        if (ok) {
            loginService.registerUser(data);
        }
    }

    $scope.recoverPassword1 = function () {
        if ($scope.recShow == false) {
            $scope.recShow = true;
        } else {
            $scope.recShow = false;
        }

    }

    $scope.recoverPassword2 = function () {
        var data = {
            'email': $scope.emailRec
        }

        if (data.email != null) {
            loginService.recover(data);
        }

    }

});
