bookapp.controller('recoverCtrl', function ($scope, $route, $window, services) {
    
    $scope.recover = function () {
        $scope.test = $route.current.params.token;
        var token = $route.current.params.token; 
        
        if ($scope.passwd != null && $scope.passwd == $scope.passwd1) {
            var data = { passwd: $scope.passwd, token: token };

            services.post('login', 'setRecPasswd', data).then(function () {
                toastr.success('Password canged');
                $window.location.href = '#/login';
            })

        } else {
            toastr.warning('Invalid password');
        }


    }

})