bookapp.controller('activateCtrl', function ($route, services, $scope, response) {
    
    if (response.correct) {
        $scope.message = 'Account activated';    
    } else {
        $scope.message = 'An error has occurred';
    }
    
})