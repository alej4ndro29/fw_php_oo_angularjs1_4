bookapp.controller('detailsCtrl', function ($scope, services, item) {
    
    var user = JSON.parse(sessionStorage.getItem('user'));

    if (user != null) {
        if (user.typeUser == 'admin') $scope.isAdmin = true;
    }

    $scope.apiLoaded = false;

    console.log(item);
    
    $scope.apiUrl = "https://www.googleapis.com/books/v1/volumes?q=" + item[0].ISBNResource;

    $scope.item = item[0];

    services.getUrl($scope.apiUrl).then(function (response) {
        console.log(response);
        $scope.apiLoaded = true;

        $scope.localItemImage = response['items'][0]['volumeInfo']['imageLinks']['thumbnail'];

        $scope.relateds = [
            {
                name: response['items'][1]['volumeInfo']['title'],
                image: response['items'][1]['volumeInfo']['imageLinks']['thumbnail'], 
                url: response['items'][1]['volumeInfo']['previewLink'],
            },
            {
                name: response['items'][2]['volumeInfo']['title'],
                image: response['items'][2]['volumeInfo']['imageLinks']['thumbnail'], 
                url: response['items'][2]['volumeInfo']['previewLink'],
            }];
    });

});