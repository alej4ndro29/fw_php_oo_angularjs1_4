bookapp.directive('headerMenu', function () {
    return {
        restrict: 'E',
        templateUrl: 'frontend/directives/headerMenu/headerMenu.html',
        controller: 'headerMenuCtrl'
    }
});

bookapp.controller('headerMenuCtrl', function ($scope, userToken, $window, services) {
    $scope.userImage = 'frontend/assets/img/defaultUser.svg';
    $scope.user = null;
    $scope.isAdminUser = false;

    var token = {userToken: userToken.getToken()};

    if (token.userToken != null) {
        userToken.getUser(token).then(function (user) {
            // console.log(JSON.stringify(user));

            sessionStorage.setItem('user' , JSON.stringify(user));

            if (user.nickname == null) {
                userToken.removeToken();
                $window.location.reload();
            } else {
                $scope.user = user;
                $scope.userImage = user.avatar;
                userToken.setToken(user.token);
                if(user.typeUser == 'admin') {
                    $scope.isAdminUser = true;
                } 
            }
        });
    }
    
    $scope.logout = function () {
        services.get('login', 'logout');
        userToken.removeToken();
        sessionStorage.removeItem('user');
        $window.location.reload();
    }

    $scope.isUserLogged = function () {
        return (token.userToken != null);
    }
    
});