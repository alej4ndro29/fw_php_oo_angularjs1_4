bookapp.directive('searchBar', function () {
    return {
        restrict: 'E',
        templateUrl: 'frontend/directives/search/searchBar.html',
        controller: 'searchBarCtrl'
    }
});


bookapp.controller('searchBarCtrl', function ($scope, $window) {

    $scope.searchBarAction = function (keyCode) {
        if(keyCode == 13 && $scope.searchBar != '' && $scope.searchBar != null) {
            $window.location = '#/search/' + $scope.searchBar;
            $scope.searchBar = '';
        }
    }

});