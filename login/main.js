var webAuth = new auth0.WebAuth({
    domain: 'alej4-dev.eu.auth0.com',
    clientID: 'HPa7flJZeZuk99N2ycB9jDC6AzCpTOlb',
    responseType: 'token id_token'
});

webAuth.parseHash(function (err, authResult) {
    window.opener.socialLoginPopup(err, authResult);
    window.close();
});